package ToDo;

import java.util.List;

import ToDo.TaskEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;



public class TaskDAO {
    SessionFactory factory = null;
    Session session = null;

    private static TaskDAO single_instance = null;

    private TaskDAO(){
        factory = HibernateUtil.getSessionFactory();
    }

    public static TaskDAO getInstance() {
        if(single_instance == null) {
            single_instance = new TaskDAO();
        }
        return single_instance;
    }


    /**
     * Save Task
     * @param task
     */
    public void saveTask(TaskEntity task) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            // save the student object
            session.save(task);
            // commit transaction
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

    /**
     * Update Task
     * @param task
     */
    public void updateTask(TaskEntity task) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            // save the student object
            session.update(task);
            // commit transaction
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

    /**
     * Update TaskCompleted
     * @param task
     */
    public void updateTaskCompleted(TaskEntity task) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            // save the student object
            session.update(task);
            // commit transaction
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

    /**
     * Delete Task
     * @param id
     */
    public void deleteTask(int id) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();

            // Delete a Task object
            TaskEntity task = session.get(TaskEntity.class, id);
            if (task != null) {
                session.delete(task);
                System.out.println("Task is deleted");
            }

            // commit transaction
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

    /**
     * Get Task By ID
     * @param id
     * @return
     */
    public TaskEntity getTask(int id) {
        TaskEntity task = null;
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            // get an Task object
            String sql = "from TaskEntity where idtask=" + Integer.toString(id);
            task = (TaskEntity)session.createQuery(sql).getSingleResult();
            // commit transaction
            session.getTransaction().commit();
            return task;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    /**
     * Get all Tasks
     * @return
     */
    @SuppressWarnings("unchecked")
    public List < TaskEntity > getAllTask() {
        List < TaskEntity > listOfTask = null;
        try  {
            session = factory.openSession();
            session.getTransaction().begin();
            // get an Task object
            listOfTask = session.createQuery("from TaskEntity").getResultList();

            // commit transaction
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
        return listOfTask;
    }
}
