package ToDo;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "task", schema = "todo")
public class TaskEntity {
    private int idtask;
    private String taskdescription;
    private LocalDate taskduedate;
    private Boolean taskcompleted;

    @Id
    @Column(name = "idtask", nullable = false)
    public int getIdtask() {
        return idtask;
    }

    public void setIdtask(int idtask) {
        this.idtask = idtask;
    }

    @Basic
    @Column(name = "taskdescription", nullable = false, length = 255)
    public String getTaskdescription() {
        return taskdescription;
    }

    public void setTaskdescription(String taskdescription) {
        this.taskdescription = taskdescription;
    }

    @Basic
    @Column(name = "taskduedate", nullable = false)
    public LocalDate getTaskduedate() {
        return taskduedate;
    }

    public void setTaskduedate(LocalDate taskduedate) {
        this.taskduedate = taskduedate;
    }

    @Basic
    @Column(name = "taskcompleted", nullable = true)
    public Boolean getTaskcompleted() {
        return taskcompleted;
    }

    public void setTaskcompleted(Boolean taskcompleted) {
        this.taskcompleted = taskcompleted;
    }

    public TaskEntity(){}

    public TaskEntity(String taskdescription, LocalDate taskduedate, Boolean taskcompleted){
        super();
        this.taskdescription = taskdescription;
        this.taskduedate = taskduedate;
        this.taskcompleted = taskcompleted;
    }

    public TaskEntity(int idtask, String taskdescription, LocalDate taskduedate, Boolean taskcompleted){
        super();
        this.idtask = idtask;
        this.taskdescription = taskdescription;
        this.taskduedate = taskduedate;
        this.taskcompleted = taskcompleted;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskEntity that = (TaskEntity) o;
        return idtask == that.idtask &&
                Objects.equals(taskdescription, that.taskdescription) &&
                Objects.equals(taskduedate, that.taskduedate) &&
                Objects.equals(taskcompleted, that.taskcompleted);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idtask, taskdescription, taskduedate, taskcompleted);
    }
}
