package ToDo;

import javax.persistence.criteria.CriteriaBuilder;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;


@WebServlet("/")
public class TaskServlet extends HttpServlet {
    private TaskDAO taskDAO;

    public void init(){
        taskDAO = TaskDAO.getInstance();
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getServletPath();

        try {
            switch (action) {
                case "/new" -> showNewForm(request, response);
                case "/insert" -> insertTask(request, response);
                case "/delete" -> deleteTask(request, response);
                case "/edit" -> showEditForm(request, response);
                case "/update" -> updateTask(request, response);
                case "/completed" -> completedTask(request, response);
                default -> listTask(request, response);
            }
        } catch (SQLException e) {
            throw new ServletException(e);
        }
    }

    private void listTask(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException, SQLException {
        List <TaskEntity> listTask = taskDAO.getAllTask();
        request.setAttribute("listTask", listTask);
        RequestDispatcher dispatcher = request.getRequestDispatcher("task-list.jsp");
        dispatcher.forward(request, response);
    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("task-form.jsp");
        dispatcher.forward(request, response);
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        int id = Integer.parseInt(request.getParameter("id"));
        TaskEntity existingTask = taskDAO.getTask(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("task-form.jsp");
        request.setAttribute("task", existingTask);
        dispatcher.forward(request, response);
    }

    private void insertTask(HttpServletRequest request, HttpServletResponse response)
            throws IOException, SQLException {
        String description = request.getParameter("taskdescription");
        LocalDate date = LocalDate.parse(request.getParameter("taskduedate"));
        Boolean completed = Boolean.valueOf(request.getParameter("taskcompleted"));
        TaskEntity newTask = new TaskEntity(description, date, completed);
        taskDAO.saveTask(newTask);
        response.sendRedirect("list");
    }

    private void updateTask(HttpServletRequest request, HttpServletResponse response)
            throws IOException, SQLException {
        int id = Integer.parseInt(request.getParameter("idtask"));
        String description = request.getParameter("taskdescription");
        LocalDate date = LocalDate.parse(request.getParameter("taskduedate"));
        Boolean completed = Boolean.valueOf(request.getParameter("taskcompleted"));
        TaskEntity task = new TaskEntity(id, description, date, completed);
        taskDAO.updateTask(task);
        response.sendRedirect("list");
    }

    private void deleteTask(HttpServletRequest request, HttpServletResponse response)
            throws IOException, SQLException {
        int id = Integer.parseInt(request.getParameter("id"));
        taskDAO.deleteTask(id);
        response.sendRedirect("list");
    }

    private void completedTask(HttpServletRequest request, HttpServletResponse response)
            throws IOException, SQLException {
        int id = Integer.parseInt(request.getParameter("id"));
        TaskEntity existingTask = taskDAO.getTask(id);
        String description = existingTask.getTaskdescription();
        LocalDate date = existingTask.getTaskduedate();
        boolean completed = existingTask.getTaskcompleted();
        completed = !completed;
        TaskEntity task = new TaskEntity(id, description, date, completed);
        taskDAO.updateTask(task);
        response.sendRedirect("list");
    }

}
