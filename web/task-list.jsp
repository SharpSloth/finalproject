<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Task Master</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/b8a0946021.js"></script>
</head>
<body>
<div class="jumbotron jumbotron-fluid text-center bg-dark text-white">
    <div class="container">
    <h1 class="display-1">Task Master</h1>
    <p>Take control of your to do list</p>
    </div>
</div>
    <div class="container">
        <a href="new">Add New Task</a></div>
<div class="container">
    <table class="table table-hover">
        <thead class="thead-dark"></thead>
        <tr>
            <th>Task</th>
            <th>Due Date</th>
            <th>Completed</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="task" items="${listTask}">
            <tr>
                <td><c:out value="${task.taskdescription}" /></td>
                <td><c:out value="${task.taskduedate}" /></td>
                <td><a href="completed?id=<c:out value='${task.idtask}' />" class="text-body">
                    <c:if test="${task.taskcompleted}"><i class="fa fa-check-square-o" ></i></c:if>
                    <c:if test="${!task.taskcompleted}"><i class="fa fa-square-o" ></i></c:if>
                </a></td>
                <td>
                    <a href="edit?id=<c:out value='${task.idtask}' />">Edit</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="delete?id=<c:out value='${task.idtask}' />">Delete</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>