<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Task Master</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
            integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="jumbotron jumbotron-fluid text-center bg-dark text-white">
    <div class="container">
        <h1 class="display-1">Task Master</h1>
        <p>Take control of your to do list</p>
    </div>
</div>
<div class="container">
            <span class="h2">
                <c:if test="${task != null}">
                    Edit Task
                </c:if>
                <c:if test="${task == null}">
                    Add New Task
                </c:if>
            </span>
</div>
<div class="container">
    <c:if test="${task != null}">
    <form action="update" method="post"></c:if>
        <c:if test="${task == null}">
        <form action="insert" method="post"></c:if>
            <c:if test="${task != null}">
                <input type="hidden" name="idtask" value="<c:out value='${task.idtask}' />"/>
            </c:if>
            <div class="form-group">
                <label for="description">Description:</label>
                <textarea class="form-control" rows="3" name="taskdescription" size="45"
                          value="<c:out value='${task.taskdescription}' />" id="description"><c:out
                        value='${task.taskdescription}'/></textarea>
            </div>
            <div class="form-group">
                <label for="duedate">Due Date:</label>
                <input type="date" name="taskduedate"
                       value="<c:out value='${task.taskduedate}' />" id="duedate">
            </div>
            <div class="form-group form-check">
                <label class="form-check-label">
                    <c:if test="${task.taskcompleted}">
                        <input type="checkbox" class="form-check-input" id="taskcompleted" name="taskcompleted"
                               value="true" checked/>
                    </c:if>
                    <c:if test="${!task.taskcompleted}">
                        <input type="checkbox" class="form-check-input" id="taskcompleted" name="taskcompleted"
                               value="true"/>
                    </c:if> Completed
                </label>
            </div>
            <input type="submit" class="btn btn-primary" action="/servlet" value="Save" />
        </form>
        <div class="contianer"><a href="list" class="h-100 align-bottom">Return to list</a></div>
</div>

</body>
</html>
